export const statuses = ['TRIAL', 'INACTIVE', 'ACTIVE'];

interface ConfigConstructor {
	corrlinks_id: string;
	amount: number;
	comment: string;
	date_created?: string;
	date_subscription_ends?: string;
	product_instance_id: number;
	product_type: string;
}

export default class Payment {
	corrlinks_id: string;
	amount: number;
	comment: string;
	date_created: string;
	date_subscription_ends?: string;
	product_instance_id: number;
	product_type: string;

	constructor(config: ConfigConstructor) {
		Object.assign(this, config);
	}

	validate(): boolean {
		// TODO
		return true;
	}
}
