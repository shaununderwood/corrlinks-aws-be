import { Pool } from 'pg';

export function PoolFactory(connectionString) {
	let pool = null;

	try {
		pool = new Pool({
			connectionString: connectionString,
		});

		// the pool with emit an error on behalf of any idle clients
		// it contains if a backend error or network partition happens
		pool.on('error', (err) => {
			console.error('Unexpected error on idle client', err);
			process.exit(-1);
		});

		return pool;
	} catch (e) {
		console.log('[pgPool.ts] error creating database Pool', e);
	}
}

let pool;

if (process && process.env) {
	if (process.env.DBUSER && process.env.DBPASS && process.env.DBHOST && process.env.DBPORT && process.env.DBNAME) {
		const connectionString = `postgres://${process.env.DBUSER}:${process.env.DBPASS}@${process.env.DBHOST}:${process.env.DBPORT}/${process.env.DBNAME}`;
		pool = PoolFactory(connectionString);
	}
}

export default pool;