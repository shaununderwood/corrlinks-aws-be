import PhonebookEntry from "./PhonebookEntry";
import Phonebook from "./Phonebook";

export default class PhonebookTransformer {
  corrlinks_id = "";
  raw = "";
  errors = [];
  linesFound = [];
  parsedEntries = [];
  phonebook = null;

  private startIdx = 0;
  private endIdx = 0;

  static ERRORS = {
    NO_PHONEBOOK: "No phonebook found",
    NO_ENTRIES: "No entries found between start and end tags",
    MISSING_START_TAG: "Please put phonebook start at the top of your list",
    MISSING_END_TAG: "Please put phonebook end at the bottom of your list.",
  };

  constructor(corrlinks_id, raw) {
    this.corrlinks_id = corrlinks_id;
    this.raw = raw.text;
    this.extractPhonebookData();
    if (this.errors.length) return null;
    if (this.parseFoundLinesToPhonebook() === true) this.phonebook;
  }
  extractPhonebookData() {
    this.errors = [];
    this.linesFound = [];

    const lines = this.extractOnlyLatestMessage(this.raw).map((a) => a.trim());
    const normalisedLines = lines.map((l) => l.toUpperCase());
    const startIdx = normalisedLines.indexOf(Phonebook.START_TAG.toUpperCase());
    const endIdx = normalisedLines.indexOf(Phonebook.END_TAG.toUpperCase());

    if (startIdx === -1 && endIdx === -1) {
      this.errors.push(PhonebookTransformer.ERRORS.NO_PHONEBOOK);
      return this;
    }
    if (startIdx === -1) {
      this.errors.push(PhonebookTransformer.ERRORS.MISSING_START_TAG);
      return this;
    }
    if (endIdx === -1) {
      this.errors.push(PhonebookTransformer.ERRORS.MISSING_END_TAG);
      return this;
    }
    if (endIdx - startIdx < 2) {
      this.errors.push(PhonebookTransformer.ERRORS.NO_ENTRIES);
      return this;
    }
    this.startIdx = startIdx + 1;
    this.endIdx = endIdx;
    console.log(`Potential entries found: ${this.endIdx - this.startIdx}`);
    for (let i = this.startIdx; i < this.endIdx; i++) {
      this.linesFound.push(lines[i]);
    }
    console.log(`extractPhonebookData`, `Found ${this.linesFound.length} lines`);
    console.log(this.linesFound);
    return true;
  }
  parseFoundLinesToPhonebook() {
    this.errors = [];
    this.parsedEntries = [];

    this.linesFound.forEach((line) => {
      let [line_id, theRest] = line.split(".");
      let [mobile, label1 = "", label2 = ""] = theRest.trim().split(" ");
      const label = (label1.trim() + " " + label2.trim()).trim().toLowerCase();
      const entry = new PhonebookEntry({ line_id, mobile, label });
      this.parsedEntries.push(entry);
    });

    const phonebook = new Phonebook(this.parsedEntries);
    phonebook.validateEntries();
    phonebook.validate();
    this.errors = [...phonebook._entryErrors, ...phonebook._errors];

    if (this.errors.length > 0) {
      return this.errors.length;
    }

    this.phonebook = phonebook;
    return true;
  }
  getPhonebook() {
    return this.phonebook;
  }
  extractOnlyLatestMessage(raw) {
    const lines = raw.split("\n");
    let message = [];
    lines.some((a) => (a.indexOf("-----") === 0 && a.indexOf("wrote:") > 0 ? true : message.push(a) && false));
    return message;
  }
}
