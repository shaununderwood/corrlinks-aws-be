import PhonebookEntry from "./PhonebookEntry";

export default class Phonebook {
  entries: PhonebookEntry[] = [];
  options = {
    maxEntries: 30,
  };

  _errors = [];
  _entryErrors = [];

  static START_TAG = "phonebook start";
  static END_TAG = "phonebook end";

  constructor(entries) {
    this.entries = entries.map((entry) => new PhonebookEntry(entry));
  }

  errors() {
    this.validateEntries();
    if (this._entryErrors.length > 0) return this._entryErrors;

    this.validate();
    if (this._errors.length > 0) return this._errors;

    return '';
  }

  validateEntries() {
    this._entryErrors = [];
    this.entries.map((entry) => {
      const errors = entry.errors();
      if (errors && errors.length) {
        this._entryErrors.push({ line_id: entry.line_id, errors });
      }
    });
  }

  validate() {
    this._errors = [];
    if (this.entries.length < 1) this._errors.push(`entries is empty`);
    if (this.entries.length > this.options.maxEntries) this._errors.push(`entries is more than ${this.options.maxEntries}, got: ${this.entries.length}`);

    const lineIdIdx = {},
      labelIdx = {},
      mobileIdx = {},
      lineErrors = {};
    const addError = (line_id, error) => {
      lineErrors[line_id] = lineErrors[line_id] || [];
      lineErrors[line_id].push(error);
    };

    this.entries.forEach((a) => {
      a.validate();
      lineIdIdx[a.line_id] = (lineIdIdx[a.line_id] || 0) + 1;
      if (lineIdIdx[a.line_id] > 1) addError(a.line_id, `is duplicated ${lineIdIdx[a.line_id]} time(s)`);
      if (a.line_id > this.options.maxEntries) addError(a.line_id, `line_id is greater than ${this.options.maxEntries}, got: ${a.line_id}`);

      if (a.label === "" && a.mobile === "") return;

      labelIdx[a.label] = (labelIdx[a.label] || 0) + 1;
      if (labelIdx[a.label] > 1) addError(a.line_id, `has a duplicate label "${a.label}"`);

      mobileIdx[a.mobile] = (mobileIdx[a.mobile] || 0) + 1;
      if (mobileIdx[a.mobile] > 1) addError(a.line_id, `has a duplicate mobile "${a.mobile}"`);
    });

    this._errors = Object.keys(lineErrors).map((line_id) => ({ line_id, errors: lineErrors[line_id] })) || [];
  }

  toText(includeTags = true) {
    let result = this.entries.map((entry) => `${entry.line_id}. ${entry.mobile}${entry.label ? " " : ""}${entry.label}`).join("\n");
    if (includeTags) result = Phonebook.START_TAG + "\n" + result + "\n" + Phonebook.END_TAG;
    return result;
  }

  isEmpty() {
    return !this.entries.some((entry) => entry.mobile !== "" && entry.label !== "");
  }
}
