export const statuses = ['TRIAL', 'INACTIVE', 'ACTIVE'];
import C from "../config/constants";

interface ConfigConstructor {
	corrlinks_id: string;
	name: string;
	date_subscription_ends: string;
	status: string;
	phonebook_entries_allowed?: number;
	use_phonebook?: number;
	date_release?: string;
}

export default class CorrlinksUser {
	corrlinks_id: string;
	name: string;
	date_subscription_ends: string;
	status: string;
	phonebook_entries_allowed: number;
	use_phonebook?: number;
	date_release?: string; // TODO validate?

	constructor(config: ConfigConstructor) {
		Object.assign(this, config);

		this.status = this.status || C.DEFAULT_USER_STATUS;
		this.phonebook_entries_allowed = this.phonebook_entries_allowed || C.DEFAULT_USER_LINES_IN_PHONEBOOK;
		this.use_phonebook = this.use_phonebook || C.DEFAULT_USER_USE_PHONEBOOK;
	}

	validate(): boolean {
		if (!this.corrlinks_id || ('' + this.corrlinks_id).length < 8) throw new Error('corrlinks_id must be 8 digits long');
		if (!this.name || this.name.length < 1) throw new Error('name required');
		if (!this.date_subscription_ends || !new Date(this.date_subscription_ends).valueOf()) throw new Error('date_subscription_ends must be a valid date');
		if (statuses.indexOf(this.status) < 0) throw new Error(`status must be one of [${statuses.map(a => `${a}`).join(', ')}]`);
		if (this.phonebook_entries_allowed < 1) throw new Error(`phonebook_entries_allowed must be greater than 0`);
		return true;
	}
}
