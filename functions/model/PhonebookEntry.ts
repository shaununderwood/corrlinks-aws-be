export default class PhonebookEntry {
  corrlinks_id;
  line_id;
  label;
  mobile;

  _errors = [];

  constructor(entry) {
    Object.assign(this, PhonebookEntry.defaultValues);
    if (!entry) return;
    this.line_id = entry.line_id;
    this.label = (entry.label || "").toLowerCase();
    this.mobile = entry.mobile || "";
  }

  validate() {
    this._errors = [];
    const { MOBILE_LENGTH } = PhonebookEntry.CONSTANTS;

    if (!this.line_id || this.line_id < 1) this._errors.push("line_id must be >= 1");

    if (!this.label && !this.mobile) return;

    if (!this.mobile || this.mobile.length !== MOBILE_LENGTH) this._errors.push(`check mobile, it must be ${MOBILE_LENGTH} digits in length`);
    else if (!this._isNumericString(this.mobile)) this._errors.push(`check mobile, it may only contain these numerical digits`);

    if (!this.label || this.label.length < 1) this._errors.push('"name" is missing, it must have 1 or more characters');
    else if (!this._isLabelString(this.label)) this._errors.push(`"name" may only contain alphabet characters`);

    if (this._errors.length) {
      // const { line_id, label, mobile } = this;
      // this._errors.push(`received: ${JSON.stringify({ line_id, label, mobile })}`);
    }
  }

  _isNumericString(string) {
    return this._isValidString(string, PhonebookEntry.CONSTANTS.MOBILE_VALID_CHARACTERS);
  }

  _isLabelString(string) {
    return this._isValidString(string, PhonebookEntry.CONSTANTS.LABEL_VALID_CHARACTERS);
  }

  _isValidString(string, validCharacters) {
    return !string.split("").some((a) => validCharacters.indexOf(a) === -1);
  }

  errors() {
    this.validate();
    if (this._errors.length === 0) return false;
    return this._errors;
  }

  static defaultValues = {
    line_id: 0,
    label: "",
    mobile: "",
  };

  static CONSTANTS = {
    MOBILE_LENGTH: 10,
    MOBILE_VALID_CHARACTERS: "1234567890",
    LABEL_VALID_CHARACTERS: " abcdefghijklmnopqrstuvwxyz",
  };
}
