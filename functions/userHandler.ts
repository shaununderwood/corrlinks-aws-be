import "../config/config";
import { APIGatewayEvent, Handler } from "aws-lambda";
import pool from "./model/pgPool";
import { MessageUtil } from "./utils/message";
import { getMethodPath } from "./utils/utils";
import UserController from "./controllers/UserController";

const GET = "GET", PUT = "PUT", POST = "POST";
import CorrlinksUser from "./model/user";

export const main: Handler = async (event: APIGatewayEvent, context) => {
  const { method, resourcePath } = getMethodPath(event);
  let userController = new UserController(pool);

  switch (resourcePath) {
    case "/users":
    case "/user":
      if (method === GET) {
        const result = await userController.getAll();
        return MessageUtil.success(result);
      }

      if (method === POST) {
        const body = JSON.parse(event.body);
        const { status, date_subscription_ends, name, corrlinks_id, phonebook_entries_allowed } = body;

        // collect corrlinks_id
        const user = new CorrlinksUser({ status, date_subscription_ends, name, corrlinks_id, phonebook_entries_allowed });
        user.validate();

        const result = await userController.insert(user);
        return MessageUtil.success(result);
      }

      break;
    case "/users/{corrlinksId}":
    case "/user/{corrlinksId}":
      if (method === GET) {
        const corrlinksId: string = event.pathParameters["corrlinksId"];
        const user = await userController.getOne(corrlinksId);
        return MessageUtil.success(user);
      }

      if (method === PUT) {
        const body = JSON.parse(event.body);
        const corrlinksId: string = event.pathParameters["corrlinksId"];
        const { corrlinks_account, location, date_subscription_ends, name, corrlinks_id, phonebook_entries_allowed, date_release } = body;

        // get user
        const user = new CorrlinksUser(await userController.getOne(corrlinksId));

        // collect corrlinks_id
        const updateUser = { corrlinks_account, location, date_subscription_ends, name, corrlinks_id, phonebook_entries_allowed, date_release };
        Object.assign(user, updateUser);
        user.validate();

        const result = await userController.update(corrlinksId, user);
        return MessageUtil.success(result);
      }
      break;
      
    default:
      break;
  }

  return MessageUtil.error(400, `Unknown method "${method}" for resource "${resourcePath}"`);
};
