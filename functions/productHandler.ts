import "../config/config";
import { Handler } from "aws-lambda";
import pool from "./model/pgPool";
import { MessageUtil } from "./utils/message";
import { getMethodPath } from "./utils/utils";
import Controller from "./controllers/ProductController";

export const main: Handler = async (event, context) => {
  const { method, path } = getMethodPath(event);

  let controller = new Controller(pool);

  switch (path) {
    case "/product":
    case "/products":
      switch (method) {
        case "GET":
          const result = await controller.getAll();
          return MessageUtil.success(result);
          break;

        default:
          break;
      }
      break;

    default:
      break;
  }

  return null;
};
