import { BaseController } from "./BaseController";
import { logger } from "../utils/utils";

const log = logger("PaymentController");

export default class PaymentController extends BaseController {
  async getAllByUser(corrlinksId) {
    const dbClient = await this.pool.connect();

    let sql = `SELECT * FROM payments WHERE corrlinks_id=$1`;
    const parameters = [corrlinksId];

    try {
      const result = await dbClient.query(sql, parameters);
      return result.rows;
    } catch (e) {
      log("error caught", e);
      log("SQL=", { sql, parameters });
      log("rethrowing error");
      throw e;
    } finally {
      dbClient.release();
    }
  }

  // async getOne(corrlinksId) {

  // }

  // async update(corrlinksId, user) {
  //   const dbClient = await this.pool.connect();
  //   const parameters = [];
  //   const fields = [];

  // }
	
  async insert(payment) {
    const dbClient = await this.pool.connect();
    const parameters = [];
    const fields = [];
		const values = [];

    Object.keys(payment).forEach( field => {
      parameters.push(payment[field]);
      fields.push(field);
			values.push(`$${fields.length}`);
    });
    let sql = `INSERT INTO payments (${fields.join(", ")}) VALUES (${ values.join(", ")})`;
		console.log(sql);

    try {
      const result = await dbClient.query(sql, parameters);
      return result;
    } catch (e) {
      log("error caught", e);
      log("SQL=", { sql, parameters });
      log("rethrowing error");
      throw e;
    } finally {
      dbClient.release();
    }
  }

}
