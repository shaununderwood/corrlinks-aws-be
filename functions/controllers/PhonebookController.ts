import { BaseController } from "./BaseController";
import { logger } from "../utils/utils";
import C from "../config/constants";
import Phonebook from "../model/Phonebook";

const log = logger("PhonebookController");

export default class PhonebookController extends BaseController {
  async get({ corrlinks_id }) {
    let sql = `
			SELECT pb.*
			FROM phonebook as pb
			INNER JOIN corrlinks_users as cu on (cu.corrlinks_id = pb.corrlinks_id)
			WHERE pb.corrlinks_id=$1 and pb.line_id <= cu.phonebook_entries_allowed
			order by line_id
		`;
    const parameters = [corrlinks_id];
    let dbClient;

    try {
      dbClient = await this.pool.connect();
      let { rows } = await dbClient.query(sql, parameters);

      if (rows.length === 0) {
        await this.create({ corrlinks_id });
        rows = await this.get({ corrlinks_id });
      }
      rows = rows.map(({ corrlinks_id, ...others }) => ({ ...others }));
      return rows;
    } catch (e) {
      log("error caught", e);
      log("SQL=", { sql, parameters });
      log("rethrowing error");
      throw e;
    } finally {
      dbClient && dbClient.release();
    }
  }

  async create({ corrlinks_id, lines = C.DEFAULT_USER_LINES_IN_PHONEBOOK }) {
    const fn = "create";
    const mobile = "",
      label = "";
    let dbClient;
    const sql = `INSERT INTO phonebook (corrlinks_id, line_id, mobile, label) VALUES ($1,$2,$3,$4)`;

    try {
      dbClient = await this.pool.connect();

      // create phonebook lines
      for (let lineNumber = 1; lineNumber <= lines; lineNumber++) {
        const parameters = [corrlinks_id, lineNumber, mobile, label];
        await dbClient.query(sql, parameters);
      }
    } catch (e) {
      console.error(fn, e);
      console.log(fn, sql);
      return e;
    } finally {
      dbClient && dbClient.release();
    }
  }

  async put(corrlinks_id: string, phonebook: Phonebook) {
    let sql = `UPDATE phonebook SET mobile=$1, label=$2 WHERE corrlinks_id=$3 AND line_id=$4 RETURNING *`;
    let dbClient;
    dbClient = await this.pool.connect();

    for (const entry of phonebook.entries) {
      console.log({ entry });
      // @ts-ignore
      const { mobile, label, line_id } = entry;
      const parameters = [mobile, label, corrlinks_id, line_id];
      let result = await dbClient.query(sql, parameters);
      if (result.rows.length > 0) {
        continue;
      }

      // if no update occurred, insert the line
      sql = `INSERT INTO phonebook (mobile, label, corrlinks_id, line_id) VALUES ($1, $2, $3, $4) RETURNING *`;
      await dbClient.query(sql, parameters);
    }

    return phonebook.entries;
  }

}
