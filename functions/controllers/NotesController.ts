import { BaseController } from "./BaseController";
import { logger } from "../utils/utils";

const log = logger("NotesController");

export default class NotesController extends BaseController {
  async getAllByUser(corrlinksId) {

    let sql = `SELECT * FROM user_notes WHERE corrlinks_id=$1`;
    const parameters = [corrlinksId];
    let dbClient;
    
    try {
      dbClient = await this.pool.connect();
      const result = await dbClient.query(sql, parameters);
      return result.rows;
    } catch (e) {
      log("error caught", e);
      log("SQL=", { sql, parameters });
      log("rethrowing error");
      throw e;
    } finally {
      dbClient.release();
    }
  }

  async delete(corrlinksId, noteId) {

    let sql = `DELETE FROM user_notes WHERE corrlinks_id=$1 AND id=$2`;
    const parameters = [corrlinksId, noteId];
    let dbClient;
    
    try {
      dbClient = await this.pool.connect();
      const result = await dbClient.query(sql, parameters);
      return result.rows;
    } catch (e) {
      log("error caught", e);
      log("SQL=", { sql, parameters });
      log("rethrowing error");
      throw e;
    } finally {
      dbClient.release();
    }
  }

  // async getOne(corrlinksId) {

  // }

  // async update(corrlinksId, user) {
  //   const dbClient = await this.pool.connect();
  //   const parameters = [];
  //   const fields = [];

  // }
	
  async insert(note) {
    const dbClient = await this.pool.connect();
    const parameters = [];
    const fields = [];
		const values = [];

    Object.keys(note).forEach( field => {
      parameters.push(note[field]);
      fields.push(field);
			values.push(`$${fields.length}`);
    });
    let sql = `INSERT INTO user_notes (${fields.join(", ")}) VALUES (${ values.join(", ")}) RETURNING *`;
		console.log(sql);

    try {
      const result = await dbClient.query(sql, parameters);
      return result.rows[0];
    } catch (e) {
      log("error caught", e);
      log("SQL=", { sql, parameters });
      log("rethrowing error");
      throw e;
    } finally {
      dbClient.release();
    }
  }

}
