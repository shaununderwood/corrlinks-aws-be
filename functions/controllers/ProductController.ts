import { BaseController } from "./BaseController";
import { logger } from "../utils/utils";

const log = logger("ProductController");

export default class ProductController extends BaseController {
  async getAll() {
    const dbClient = await this.pool.connect();

    let sql = `
      SELECT 
        products.id AS product_id,
        products.name AS product_name,
        products.type,
        product_instances.id AS product_instances_id,
        product_instances.cost,
        product_instances.months
      FROM products
        INNER JOIN product_instances ON (products.id = product_instances.product_id)
      WHERE
        (products.date_deleted IS null OR products.date_deleted IS NULL OR products.date_deleted > CURRENT_TIMESTAMP)
        AND
        (product_instances.date_deleted IS NULL OR product_instances.date_deleted IS NULL OR product_instances.date_deleted > CURRENT_TIMESTAMP)
      ORDER BY products.id, product_instances.id
    `;
    const parameters = [];

    try {
      const result = await dbClient.query(sql, parameters);
      const allProducts = result.rows.reduce((a, product) => {
        const t = Object.assign({}, a);
        const { product_id, product_name, type, date_created, date_deleted } = product;
        t[product_id] ? t[product_id] : (t[product_id] = { product_id, product_name, type, date_created, date_deleted, instances: [] });

        const { product_instances_id, months, cost } = product;
        const instance = { product_instances_id, months, cost };
        t[product_id].instances.push(instance);

        return t;
      }, {});

      return Object.keys(allProducts).map((key) => allProducts[key]);
    } catch (e) {
      log("error caught", e);
      log("SQL=", { sql, parameters });
      log("rethrowing error");
      throw e;
    } finally {
      dbClient.release();
    }
  }

  async getOne(product_instance_id) {
    const dbClient = await this.pool.connect();
    let sql = `SELECT * FROM product_instances WHERE id=$1`;
    const parameters = [product_instance_id];
    try {
      const result = await dbClient.query(sql, parameters);
      return result.rows[0];
    } catch (e) {
      log("error caught", e);
      log("SQL=", { sql, parameters });
      log("rethrowing error");
      throw e;
    } finally {
      dbClient.release();
    }
  }
}
