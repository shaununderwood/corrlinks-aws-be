import { BaseController } from "./BaseController";
import { logger } from "../utils/utils";

const log = logger("UserController");

export default class UserController extends BaseController {
  async getAll() {
    const dbClient = await this.pool.connect();

    let sql = `SELECT * FROM corrlinks_users`;
    const parameters = [];

    try {
      const result = await dbClient.query(sql, parameters);
      return result.rows;
    } catch (e) {
      log("error caught", e);
      log("SQL=", { sql, parameters });
      log("rethrowing error");
      throw e;
    } finally {
      dbClient.release();
    }
  }

  async getOne(corrlinksId) {
    const dbClient = await this.pool.connect();

    let sql = `SELECT * FROM corrlinks_users WHERE corrlinks_id=$1`;
    const parameters = [corrlinksId];

    try {
      const result = await dbClient.query(sql, parameters);
      return result.rows[0];
    } catch (e) {
      log("error caught", e);
      log("SQL=", { sql, parameters });
      log("rethrowing error");
      throw e;
    } finally {
      dbClient.release();
    }
  }

  async update(corrlinksId, user) {
    const dbClient = await this.pool.connect();
    const parameters = [];
    const fields = [];

    Object.keys(user).forEach( field => {
      if (field == "corrlinks_id") return;
      parameters.push(user[field]);
      fields.push(`${field}=$${parameters.length}`);
    });
    let sql = `UPDATE corrlinks_users SET ${fields.join(",")} WHERE corrlinks_id='${corrlinksId}'`;

    try {
      const result = await dbClient.query(sql, parameters);
      return result;
    } catch (e) {
      log("error caught", e);
      log("SQL=", { sql, parameters });
      log("rethrowing error");
      throw e;
    } finally {
      dbClient.release();
    }
  }
	
  async insert(user) {
    const dbClient = await this.pool.connect();
    const parameters = [];
    const fields = [];
		const values = [];

    Object.keys(user).forEach( field => {
      parameters.push(user[field]);
      fields.push(field);
			values.push(`$${fields.length}`);
    });
    let sql = `INSERT INTO corrlinks_users (${fields.join(", ")}) VALUES (${ values.join(", ")})`;
		console.log(sql);

    try {
      const result = await dbClient.query(sql, parameters);
      return result;
    } catch (e) {
      log("error caught", e);
      log("SQL=", { sql, parameters });
      log("rethrowing error");
      throw e;
    } finally {
      dbClient.release();
    }
  }

}
