import { Context } from "aws-lambda";
import { MessageUtil } from "../utils/message";

export class BaseController {
  protected pool;
  constructor(pool = null){
    this.pool = pool;
  }
  test(event: any, context?: Context) {
    console.log("functionName", context.functionName);

    try {
      const result = {
        message: "test ok",
        functionName: context.functionName
      };
      return MessageUtil.success(result);
    } catch (err) {
      console.error(err);

      return MessageUtil.error(err.code, err.message);
    }
  }
}
