import "../config/config";
import { APIGatewayEvent, Handler } from "aws-lambda";
import pool from "./model/pgPool";
import { MessageUtil } from "./utils/message";
import { getBodyFromEvent, getMethodPath } from "./utils/utils";
import NotesController from "./controllers/NotesController";
import UserController from "./controllers/UserController";
import SettingsController from "./controllers/SettingsController";

interface ExtractedUserDetails {
  corrlinks_id: string;
  name: string
  corrlinks_account?: string;
  date_subscription_ends?: Date;
  status?: string;
  use_phonebook?: number;
}

interface RequestBody {
  to: string;
  from: string;
  body: string;
  subject: string;
  account: string;
}

const USER_STATUS = {
	BLOCKED: 'BLOCKED',
	TRIAL: 'TRIAL',
	LIVE: 'LIVE',
};


function extractUserObject(a: string): ExtractedUserDetails {
	const bits1 = a.split('(');
	if (bits1.length !== 2) throw new Error(`Unexpected structure to 'from' "${a}"`);
	const name = bits1[0].trim();
	const corrlinks_id = bits1[1].substring(0, 8);
	return { name, corrlinks_id };
}

export const main: Handler = async (event: APIGatewayEvent, context) => {
  const { method, resourcePath } = getMethodPath(event);

  const fn = `[${method}]messageFromCorrlinksHandler`;

  try {
    const requestBody: RequestBody = getBodyFromEvent(event);
    const { from, to, body, subject, account } = requestBody;
    const original_message = body;
    let newUserAction;

    // validate
    const errors = [];
    if (!from) errors.push({ from: 'required' });
    if (!to) errors.push({ to: 'required' });
    if (!body) errors.push({ body: 'required' });
    if (typeof (subject) !== 'string' || subject === "") errors.push({ subject: 'required' });
    if (typeof (account) !== 'string' || account === "" || account.indexOf('@') < 1) errors.push({ account: 'required and must be an email address' });
    // return result
    if (errors.length) {
      throw errors;
    }

    // ignore blank messages
    if (body.trim().length === 0) {
      throw 'failed. message is blank';
    }

    // collect user details
    const extractedUserDetails: ExtractedUserDetails = extractUserObject(from);
    const userController = new UserController(pool);
    const user = await userController.getOne(extractedUserDetails.corrlinks_id);
    // if (!user) {
    //   throw new Error(`Unknown user with id ${extractedUserDetails.corrlinks_id}`);
    // }

    // add corrlinks account
    extractedUserDetails.corrlinks_account = account.toLowerCase();

    const settingsController = new SettingsController(pool);

    // create user if they dont exists
    if (!user) {
      console.log(`user (${extractedUserDetails.corrlinks_id}) not found.  Treat as new user.`);
      extractedUserDetails.date_subscription_ends = new Date();

      // give them a free 72 hour window
      let trialWindowHours = await settingsController.getSetting('trial-window-in-hours', { hours: 72 });
      extractedUserDetails.date_subscription_ends.setHours(extractedUserDetails.date_subscription_ends.getHours() + trialWindowHours.hours);
      extractedUserDetails.status = USER_STATUS.TRIAL;

      // set use_phonebook to true if some or all users use the phonebook, that is we default to using the phonebook
      const enabledPhonebookOnAllUsers = await settingsController.getSetting('enabled-phonebook-on-all-users', { value: false });
      const enabledPhonebookOnSomeUsers = await settingsController.getSetting('enabled-phonebook-on-some-users', { value: false });
      extractedUserDetails.use_phonebook = enabledPhonebookOnSomeUsers.value || enabledPhonebookOnAllUsers.value ? 1 : 0;

      
      return MessageUtil.success({status: 'Got this far', extractedUserDetails});

      // create new record
      await routingDatabase.userCreate(extractedUserDetails);

      // retrieve user record
      user = await routingDatabase.userById(extractedUserDetails.corrlinks_id);

      // retrieve welcome message and send it
      const setting = await routingDatabase.getSetting('welcome-message');
      const to = user.corrlinks_id;
      let { from, subject, body } = setting;
      const subscription_end_date = ` ${extractedUserDetails.date_subscription_ends.toLocaleDateString()} ${extractedUserDetails.date_subscription_ends.toLocaleTimeString()}`;
      body = body.replace('{{subscription_end_date}}', subscription_end_date);
      await routingDatabase.addMessageForCorrlinks({ from, to, body, subject });

      // send phonebookblank phonebook message if use_phonebook is true
      if (parseInt(user.use_phonebook, 10) === 1) {
        newUserAction = 'phonebook';
      }
    }
    return MessageUtil.success({status: 'Got this far. Got user, so skipped to here', user});

    // // data migration, add any missing corrlinks_account values
    // if ((!user.corrlinks_account || user.corrlinks_account.trim() === '') && extractedUserDetails.corrlinks_account) {
    //   await this.updateCorrlinksAccountIfBlank(user, extractedUserDetails);
    // }

    // if (user.status === 'INACTIVE') {
    //   console.log(fn, `${user.corrlinks_id} INACTIVE, skipping`);
    //   return res.status(200).json({ statusCode: 402, data: { status: 'failed. user inactive' } });
    // }

    // // perform subscription checks if enabled
    // let enabledSubscriptionFunctionality = await routingDatabase.getSetting('enabled-subscription-functionality', { value: false });
    // if (enabledSubscriptionFunctionality.value === true) {

    //   // if their subscription has expired, setup a return message stating this
    //   if ((new Date(user.date_subscription_ends)).valueOf() < (new Date()).valueOf()) {
    //     console.log(fn, `user (${user.corrlinks_id}) subscription expired`);

    //     // get settings
    //     const setting = await routingDatabase.getSetting('payment-required-message-inmate');

    //     // queue message
    //     const to = user.corrlinks_id;
    //     const { from, subject, body } = setting;
    //     await routingDatabase.addMessageForCorrlinks({ from, to, body, subject });

    //     return res.status(200).json({ statusCode: 402, data: { status: `user (${user.corrlinks_id}) subscription expired` } }); // 402 PAYMENT REQUIRED
    //   }
    // } else {
    //   console.log(fn, 'enabled-subscription-functionality is false, using isUserBlocked instead');

    //   // if user is blocked we ignore them
    //   // their name can come either way around, apparently
    //   let nameForward = user.name, nameBackward = user.name.split(' ').reverse().join(' ');

    //   if (await routingDatabase.isUserBlocked(nameForward) || await routingDatabase.isUserBlocked(nameBackward)) {
    //     console.log(fn, `user ${user.corrlinks_id} status is blocked`);
    //     return res.status(200).json({ statusCode: 410, data: { status: 'failed. user is blocked' } }); // 410 GONE
    //   }
    // }

    // // check if system is using the phonebook for all messages, just on a user basis, or not at all
    // const enabledPhonebookOnAllUsers = await routingDatabase.getSetting('enabled-phonebook-on-all-users', { value: false });
    // const enabledPhonebookOnSomeUsers = await routingDatabase.getSetting('enabled-phonebook-on-some-users', { value: false });

    // // check if system is using the phonebook
    // if (enabledPhonebookOnAllUsers.value || (enabledPhonebookOnSomeUsers.value && parseInt(user.use_phonebook) === 1)) {
    //   console.log('Phonebook: using');

    //   // collect phonebook label
    //   let [label, errors, action] = getLabelFromString(subject);
    //   console.log(`label="${label}", errors="${errors}", action="${action}"`);

    //   // if label is a reservered word, action it
    //   action = newUserAction || action;
    //   if (action || newUserAction) {
    //     console.log(`Phonebook: Action found: ${action}`)
    //     if (action === 'phonebook') {
    //       return this.processPhonebook({ user, action, body, res });
    //     }

    //     // actions help and info
    //     if (['help', 'info'].indexOf(action) >= 0) {
          
    //       const note = `${action}:\n${original_message}`;
    //       const corrlinks_id = user.corrlinks_id;
    //       const newNote = new UserNote({ corrlinks_id, note });
          
    //       if (newNote.validate()){
    //         // save note to database
    //         await routingDatabase.addUserNote({ note: newNote });
    //         return res.status(200).json({ statusCode: 200, data: { status: `action "${action}" received.` } });
    //       }
    //     }

    //     return res.status(400).json({ statusCode: 400, data: { status: `Action "${action}" not implemented` } });
    //   }

    //   // if errors found in label
    //   if (errors) {
    //     console.log('Phonebook: errors in label', errors);

    //     // get settings
    //     const setting = await routingDatabase.getSetting('label-not-found-message-to-inmate');

    //     // get phoonebook
    //     const phonebook = getPhonebookAsText({ corrlinks_id: user.corrlinks_id });

    //     // queue message
    //     const to = user.corrlinks_id;
    //     let { from, body } = setting;
    //     body = body.replace('{{errors}}', errors);
    //     body = body.replace('{{original_message}}', original_message);
    //     body = body.replace('{{phonebook}}', phonebook);
    //     await routingDatabase.addMessageForCorrlinks({ from, to, body, subject });

    //     return res.status(402).json({ statusCode: 402, data: { status: errors } });
    //   }

    //   // get phonebook entry
    //   const entry = await routingDatabase.getUserPhonebookEntry({ corrlinks_id: user.corrlinks_id, label });

    //   // if no entry found, send the user their phonebook back with an error message
    //   if (!entry) {
    //     console.log(`Phonebook: entry for "${label}" not found`);

    //     // get settings
    //     const setting = await routingDatabase.getSetting('label-not-found-in-phonebook-message-to-inmate');

    //     // queue message
    //     const to = user.corrlinks_id;
    //     let { from, body } = setting;
    //     body = body.replace('{{label}}', label);
    //     body = body.replace('{{original_message}}', original_message);
    //     await routingDatabase.addMessageForCorrlinks({ from, to, body, subject });

    //     return res.status(404).json({ statusCode: 404, data: { status: body } }); // 404 NOT FOUND
    //   }

    //   // set to: to entry
    //   to = entry.mobile;

    // } else {
    //   // system isn't using the phonebook || this user isn't using the phonebook
    //   to = extractMobileNumber(body) || "";
    //   if (!to) return res.status(400).json({ statusCode: 400, data: { status: 'Phonebook not in use, and no mobile number on the first line. Message discarded' } }); // 404 CLIENT ERROR
    // }

    // // adjust 'to' to include '+1'
    // if (to.indexOf('+1') !== 0) {
    //   to = `+1${to}`;
    // }
    // to = to.split(' ').join('').split('-').join('');

    // // save message to database
    // await routingDatabase.addMessageForSociety({ from: user.corrlinks_id, to, body, subject });
    // console.log(JSON.stringify({ from: user.corrlinks_id, to, body, subject }));
    // console.debug(fn, 'message saved');
    return MessageUtil.success({status: 'sent'});

  } catch (e) {
    console.error(fn, e);
    return MessageUtil.error(400, e);
  } finally {

  }

  return MessageUtil.error(400, `Unknown method "${method}" for resource "${resourcePath}"`);
};
