import "../config/config";
import { APIGatewayEvent, Handler } from "aws-lambda";
import pool from "./model/pgPool";
import { MessageUtil } from "./utils/message";
import { getMethodPath } from "./utils/utils";
import PaymentController from "./controllers/PaymentController";
import UserController from "./controllers/UserController";
// import Payment from "./model/payment";
// import ProductController from "./controllers/ProductController";

const GET = "GET",
  // PUT = "PUT",
  POST = "POST";

function getBody(event) {
  return JSON.parse(event.body);
}

export const main: Handler = async (event: APIGatewayEvent, context) => {
  const { method, resourcePath } = getMethodPath(event);

  switch (resourcePath) {
    case "/users/{corrlinksId}/payments":
      if (method === GET) {
        // check user
        const corrlinksId: string = event.pathParameters["corrlinksId"];
        const userController = new UserController(pool);
        const user = await userController.getOne(corrlinksId);
        if (!user) {
          throw new Error(`Unknown user with id ${corrlinksId}`);
        }

        const paymentController = new PaymentController(pool);
        const data = await paymentController.getAllByUser(corrlinksId);

        return MessageUtil.success(data);
      }

      if (method === POST) {
        const fn = `${resourcePath}: ${method === POST}`;
        try {
          // collect corrlinks_id
          const corrlinksId: string = event.pathParameters["corrlinksId"];
          const userController = new UserController(pool);
          const user = await userController.getOne(corrlinksId);
          if (!user) {
            throw new Error(`Unknown user with id ${corrlinksId}`);
          }

          const body = getBody(event);
          const { amount, comment, date_created, duration_in_months = null } = body;
          let { date_subscription_ends = null } = body;

          // date_subscription_ends && duration_in_months are mutually exclusive
          if (date_subscription_ends && duration_in_months) {
            throw new Error("fields date_subscription_ends and duration_in_months are mutually exclusive");
          }
          // if (!date_subscription_ends && !duration_in_months) {
          // 	throw new Error('one of date_subscription_ends and duration_in_months must be set');
          // }
          if (!isNaN(parseInt(duration_in_months)) && duration_in_months < 1) {
            throw new Error("Invalid duration_in_months. duration_in_months must be >= 1");
          }

          // base properties for all payments
          let payment = { corrlinks_id: corrlinksId, amount, comment, date_created };

          // set date_subscription_ends to `duration` months ahead of existing date_subscription_ends,
          //   or from today if date_subscription_ends has passed
          if (duration_in_months) {
            let newSubscriptionDate = new Date(user.date_subscription_ends);
            const now = new Date();
            if (newSubscriptionDate.toString() === "Invalid Date" || newSubscriptionDate < now) {
              newSubscriptionDate = now;
            }
            newSubscriptionDate.setMonth(newSubscriptionDate.getMonth() + duration_in_months);
            date_subscription_ends = newSubscriptionDate.toJSON().substr(0, 10);

            // update payment with comment
            payment = Object.assign(payment, {
              comment: `added ${duration_in_months} months to subscription, date_subscription_ends: ${date_subscription_ends}`,
            });
          }

          // save payment to database
          const paymentController = new PaymentController(pool);
          const dbResultsPayment = await paymentController.insert(payment);

          // update user subscription and status
          let dbResultsSubscription;
          if (date_subscription_ends !== null) {
            user.date_subscription_ends = date_subscription_ends;
          }
          if (user.status !== "INACTIVE") {
            // this looks odd, but its working
            user.status = "ACTIVE";
          }
          dbResultsSubscription = await userController.update(corrlinksId, user);
          const result = MessageUtil.success({ dbResultsPayment, dbResultsSubscription });
          return result;
        } catch (e) {
          console.error(fn, e);
          return MessageUtil.error(500, e.message);
        }
      }
      break;

    case "/user/{corrlinksId}/purchase/{productInstanceId}":
      // if (method === POST) {
      //   try {
      //     // validate user
      //     const corrlinksId: string = event.pathParameters["corrlinksId"];
      //     const userController = new UserController(pool);
      //     const user = await userController.getOne(corrlinksId);
      //     if (!user) {
      //       throw new Error(`Unknown user with id ${corrlinksId}`);
      //     }
      //     // validate product instance
      //     const productInstanceId = event.pathParameters["productInstanceId"];
      //     const productController = new ProductController(pool);
      //     const product_instance = await productController.getOne(productInstanceId);
      //     if (!product_instance) throw Error(`Unknown product instance id ${productInstanceId}`);

      //     // body
      //     const body = JSON.parse(event.body);
      //     const { amount = 0, comment = "" } = body;

      //     // switch to the correct routine to complete purchase
      //     const paymentController = new PaymentController(pool);

      //     switch (product_instance.type) {
      //       case "phonebook":
      //         var results = await paymentController.doPhonebookPurchase({ user, product_instance, amount, comment });
      //         break;
      //       case "monthly_subscription":
      //         var results = await paymentController.doSubscriptionPurchase({ user, product_instance, amount, comment });
      //         break;
      //       default:
      //         var results = await paymentController.doAdhocPurchase({ user, amount }); // this is not used, the frontend send the request to the previous endpoint
      //         break;
      //     }

      //     if (!results) {
      //       return MessageUtil.error(400, "unknown error, check source code");
      //     }
      //     return MessageUtil.success(results);
      //   } catch (e) {
      //     console.error(e);
      //     return MessageUtil.error(500, e.message);
      //   }
      // }

      break;
    default:
      break;
  }

  return MessageUtil.error(400, `Unknown method "${method}" for resource "${resourcePath}"`);
};

// async function doSubscriptionPurchase({ user, product_instance, amount, comment }) {
//   // get current sub end date
//   let newSubscriptionDate = new Date(user.date_subscription_ends);
//   const now = new Date();

//   // if sub date is in the past, make it today
//   if (newSubscriptionDate.toString() === "Invalid Date" || newSubscriptionDate < now) {
//     newSubscriptionDate = now;
//   }

//   // increase subscription end date by months based on product instance
//   newSubscriptionDate.setMonth(newSubscriptionDate.getMonth() + product_instance.months);
//   const date_subscription_ends = newSubscriptionDate.toJSON().substr(0, 10);

//   // reactivate user if inactive
//   user.date_subscription_ends = date_subscription_ends;
//   if (user.status !== "INACTIVE") {
//     // this looks odd, but its working
//     user.status = "ACTIVE";
//   }

//   // save payment details
//   const { corrlinks_id } = user;
//   const date_created = new Date().toJSON().substr(0, 10);
//   const payment = { corrlinks_id, amount, comment: `${comment}\nadded ${product_instance.months} months to messaging subscription`, date_created };
//   const dbResultsPayment = await paymentController.insert(payment);

//   // update subscription details
//   const dbResultsSubscription = await userController.update(corrlinks_id, user);

//   return { dbResultsPayment, dbResultsSubscription };
// }

// async function doPhonebookPurchase({ user, product_instance, amount, comment }) {
//   // get current sub end date
//   let newSubscriptionDate = new Date(user.date_phonebook_subscription_ends);
//   const now = new Date();

//   // if sub date is in the past, make it today
//   if (newSubscriptionDate.toString() === "Invalid Date" || newSubscriptionDate < now) {
//     newSubscriptionDate = now;
//   }

//   // increase subscription end date by months based on product instance
//   newSubscriptionDate.setMonth(newSubscriptionDate.getMonth() + product_instance.months);
//   date_phonebook_subscription_ends = newSubscriptionDate.toJSON().substr(0, 10);

//   // reactivate user if inactive
//   user.date_phonebook_subscription_ends = date_phonebook_subscription_ends;
//   user.phonebook_entries_allowed = 30;

//   // save payment details
//   const { corrlinks_id } = user;
//   const date_created = new Date().toJSON().substr(0, 10);
//   const payment = { corrlinks_id, amount, comment: `${comment}\nadded ${product_instance.months} months to phonebook subscription`, date_created };
//   dbResultsPayment = await routingDatabase.saveUserPayment({ payment });

//   // update subscription details
//   const dbResultsSubscription = await routingDatabase.putUser({ corrlinks_id, user });

//   return { dbResultsPayment, dbResultsSubscription };
// }

// async function doAdhocPurchase({ user, amount }) {
//   return "doAdhocPurchase: not yet implemented";
// }
