export function getMethodPath(event: any) {
  const resourcePath = "/" + event.resource.split("/").slice(2).join("/");
  return {
    method: event.httpMethod,
    path: event.path,
    resourcePath,
  };
}

export function logger(className) {
  return (...text) => console.log(`[${className}]: `, ...text);
}

export function getEventPathVariable(event, parameterName: string): any {
  return event && event.pathParameters && event.pathParameters[parameterName] || null;
}

export function getBodyFromEvent(event) {
  return JSON.parse(event.body);
}
