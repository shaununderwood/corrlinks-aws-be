import "../config/config";
import { APIGatewayEvent, Handler } from "aws-lambda";
import pool from "./model/pgPool";
import { MessageUtil } from "./utils/message";
import { getBodyFromEvent, getMethodPath } from "./utils/utils";
import NotesController from "./controllers/NotesController";
import UserController from "./controllers/UserController";

const GET = "GET";
const POST = "POST";
const DELETE = "DELETE";

export const main: Handler = async (event: APIGatewayEvent, context) => {
  const { method, resourcePath } = getMethodPath(event);

  // check user
  const corrlinksId: string = event.pathParameters["corrlinksId"];
  const userController = new UserController(pool);
  const user = await userController.getOne(corrlinksId);
  if (!user) {
    throw new Error(`Unknown user with id ${corrlinksId}`);
  }

  const fn = `[${method}]notesHandler`;

  switch (method) {
    case GET: {
      try {
        const notesController = new NotesController(pool);
        const corrlinksId: string = event.pathParameters["corrlinksId"];
        const data = await notesController.getAllByUser(corrlinksId);
        return MessageUtil.success(data);
      } catch (e) {
        console.error(fn, e);
        return MessageUtil.error(500, e.message);
      }
      break;
    }
    case POST: {
      try {
        // check user
        const corrlinks_id: string = event.pathParameters["corrlinksId"];
        const body = getBodyFromEvent(event);
        const { note } = body;
        if (!note) throw new Error(`missing note`);
        const newNote = {
          corrlinks_id,
          note,
        };
        const notesController = new NotesController(pool);
        const data = await notesController.insert(newNote);
        const result = MessageUtil.success(data);
        return result;
      } catch (e) {
        console.error(fn, e);
        return MessageUtil.error(500, e.message);
      }
      break;
    }

    case DELETE: {
      try {
        // check user
        const corrlinks_id: string = event.pathParameters["corrlinksId"];
        const note_id: string = event.pathParameters["noteId"];
        const notesController = new NotesController(pool);
        await notesController.delete(corrlinks_id, note_id);
        const data = await notesController.getAllByUser(corrlinks_id);
        return data;
      } catch (e) {
        console.error(fn, e);
        return MessageUtil.error(500, e.message);
      }
      break;
    }

    default:
      break;
  }

  return MessageUtil.error(400, `Unknown method "${method}" for resource "${resourcePath}"`);
};
