import "../config/config";

import { Handler } from "aws-lambda";
import PhonebookController from "./controllers/PhonebookController";
import pool from "./model/pgPool";
import { MessageUtil } from "./utils/message";
import { getMethodPath, getEventPathVariable, getBodyFromEvent } from "./utils/utils";
import UserController from "./controllers/UserController";
import Phonebook from "./model/Phonebook";

const GET = "GET";
const PUT = "PUT";

export const main: Handler = async (event, context) => {
  const { method } = getMethodPath(event);

  // check user
  const corrlinksId: string = event.pathParameters["corrlinksId"];
  const userController = new UserController(pool);
  const user = await userController.getOne(corrlinksId);
  if (!user) {
    throw new Error(`Unknown user with id ${corrlinksId}`);
  }

  const fn = `[${method}]phonebookHandler`;

  let phonebookController = new PhonebookController(pool);

  switch (method) {
    case GET: {
      try {
        const corrlinks_id = getEventPathVariable(event, "corrlinksId");
        const result = await phonebookController.get({ corrlinks_id });
        return MessageUtil.success(result);
      } catch (e) {
        console.error(fn, e);
        return MessageUtil.error(500, e.message);
      }
      break;
    }

    case PUT: {
      try {
        const corrlinks_id = getEventPathVariable(event, "corrlinksId");
        const body = getBodyFromEvent(event);
        const phonebook = new Phonebook(body);
        const errors = phonebook.errors();
        if (errors.length > 0) {
          throw errors;
        }

        // save phonebook
        const result = await phonebookController.put(corrlinks_id, phonebook);

        return MessageUtil.success(result);
      } catch (e) {
        console.error(fn, e);
        return MessageUtil.error(400, e);
      }
      break;
    }

    default:
      break;
  }

  return null;
};
