import { Handler } from 'aws-lambda';
import { TestController } from './controllers/TestController';

export const main: Handler = async (event, context) => {
	const controller = new TestController();
	return controller.test(event, context);
}